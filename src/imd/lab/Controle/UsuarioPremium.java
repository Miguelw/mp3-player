package imd.lab.Controle;

import imd.lab.Exception.ArquivoException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class UsuarioPremium extends Usuario {

    private ArrayList<PlayList> playlist;

    public UsuarioPremium(String nome, String login, String senha) {
        super(nome, login, senha);
        this.playlist = new ArrayList<>();
    }

    public ArrayList<PlayList> getPlaylist() {
        return playlist;
    }

    public void adicionarPlaylist(String nomePlayList) {
        removerPlaylist(nomePlayList);
        this.playlist.add(new PlayList(nomePlayList));
    }

    public void salvarPlaylist(String nomePlaylist) throws ArquivoException {
        String nomeFinal = ("lib\\" + super.getLogin().concat(super.getNome().concat("-" + nomePlaylist) + ".txt"));
        File arq = new File(nomeFinal);
        try {
            arq.createNewFile();
        } catch (IOException ex) {
            throw new ArquivoException("Erro ao criar Arquivo de PlayList");
        }

    }

    public PlayList removerPlaylist(String nomePlaylist) {
        for (int cont = 0; cont < playlist.size(); cont++) {
            if (playlist.get(cont).getNome().toLowerCase().equals(nomePlaylist)) {
                boolean apagado = apagarArquivo(nomePlaylist);
                if (apagado) {
                    return playlist.remove(cont);
                }
            }
        }
        return null;
    }

    private boolean apagarArquivo(String nomeArquivo) {
        File arquivos[];
        File diretorio = new File("lib\\");
        arquivos = diretorio.listFiles();

        for (File arquivo : arquivos) {
            if ((arquivo.toPath().getFileName()).toString().toLowerCase().equals(super.getLogin().concat(super.getNome().concat("-" + nomeArquivo) + ".txt"))) {
                return arquivo.delete();
            }
        }
        return false;
    }

    public void adicionarMusicaPlaylist(String nomePlaylist, ArrayList<String> musicas) {
        playlist.stream().filter((playList) -> (playList.getNome().equals(nomePlaylist))).forEach((playList) -> {
            playList.adicionarMusicas(musicas);
        });
    }
}

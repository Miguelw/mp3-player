package imd.lab.Controle;

import java.io.File;
import javazoom.jl.decoder.JavaLayerException;

public class ReprodutorMP3 {

    private static MP3 mp3;
    Thread threadPlayer;

    /*
        Estados de uma thread
        nova vazia -- "NEW"
        tocando -- "TIMED_WAITING"
        PARADO -- "TERMINATED"
     */
    /**
     *
     * @param mp3 File arquivo contendo caminho da musica
     * @throws JavaLayerException Exceção da classe JLayer
     */
    public ReprodutorMP3(File mp3) throws JavaLayerException {
        this.mp3 = new MP3(mp3);
    }

    /**
     * Inicia o player com uma nova thread, ou continua se a thread atual se
     * encontra parada
     */
    public void play() {
        if (threadPlayer == null || threadPlayer.getState() == Thread.State.valueOf("TERMINATED")) {
            this.threadPlayer = new Thread(mp3, "musica");
            threadPlayer.start();
        } else {
            threadPlayer.resume();
        }

    }

    /**
     * Suspende a thread atual
     */
    public void pause() {
        if (threadPlayer.getState() == Thread.State.valueOf("TIMED_WAITING")) {
            threadPlayer.suspend();
        }
    }

    /**
     * Para a reprodução e remove o player da thread
     */
    public void stop() {
        mp3.stop();
        System.gc();
    }

}

package imd.lab.Controle;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javax.swing.JOptionPane;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class MP3 implements Runnable {

    private String STATUS_PLAYER;
    private final File music;
    private Player player;

    public MP3(File music) throws JavaLayerException {
        this.music = music;
        STATUS_PLAYER = "NAOEXECUTADO";
    }

    public void stop() {
        STATUS_PLAYER = "NAOEXECUTADO";
        player.close();
    }

    public void play() {

        if (STATUS_PLAYER.equals("NAOEXECUTADO")) {
            try {
                FileInputStream stream = new FileInputStream(music);
                BufferedInputStream buffer = new BufferedInputStream(stream);

                player = new Player(buffer);
            } catch (JavaLayerException ex) {
                JOptionPane.showMessageDialog(null, "Erro ao executar arquivo", "ERRO", JOptionPane.ERROR_MESSAGE);
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Arquivo Não encontrado", "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        }
        try {
            STATUS_PLAYER = "EXECUTANDO";
            player.play();
        } catch (JavaLayerException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao executar arquivo", "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void run() {
        play();
    }

}

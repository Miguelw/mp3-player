package imd.lab.Controle;

import java.util.ArrayList;

public class PlayList {

    private String nome;
    private int qtdMusica;
    private ArrayList<String> musicas;

    public PlayList(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdMusica() {
        return qtdMusica;
    }

    public void setQtdMusica(int qtdMusica) {
        this.qtdMusica = qtdMusica;
    }

    public ArrayList<String> getMusicas() {
        return musicas;
    }

    public void adicionarMusicas(ArrayList<String> musicas) {
        this.musicas = musicas;
    }

}

package imd.lab.Exception;

public class ArvoreException extends Exception {

    public ArvoreException(String message) {
        super(message);
    }

}

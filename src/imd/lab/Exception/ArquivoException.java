package imd.lab.Exception;

public class ArquivoException extends Exception {

    public ArquivoException(String message) {
        super(message);
    }

}

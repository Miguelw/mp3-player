package imd.lab.Estrutura;

import imd.lab.Controle.Usuario;
import imd.lab.Exception.ArquivoException;
import imd.lab.Exception.ArvoreException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JOptionPane;

/**
 *
 * @author Miguel
 * @version 0.3
 */
public class ArvoreBinaria {

    private No raiz;

    /**
     * Construtor da arvore Binária
     */
    public ArvoreBinaria() {
    }

    public No getRaiz() {
        return raiz;
    }

    public void setRaiz(No raiz) {
        this.raiz = raiz;
    }

    public int getAltura() {
        return getAltura(this.raiz);
    }

    /**
     *
     * @param no No que serÃ¡ contado recursivamente
     * @return int maior altura entre o nÃ³ direito e esquerdo
     */
    private int getAltura(No no) {
        if (no == null) {
            return 0;
        }
        int altEsq = getAltura(no.getEsquerda());
        int altDir = getAltura(no.getDireita());
        if (altEsq > altDir) {
            return altEsq + 1;
        } else {
            return altDir + 1;
        }
    }

    /**
     * Wrapper para Buscar
     *
     * @param login Nome a ser buscada
     * @param senha senha de verificação
     * @return No contendo as informações passadas por parâmetro
     * @throws imd.lab.Exception.ArvoreException retorna exceção caso não
     * encontre o usuario
     */
    public No buscar(String login, String senha) throws ArvoreException {
        No encontrado = buscar(raiz, login);
        if (encontrado.getInfo().getSenha().equals(senha)) {
            return encontrado;
        }
        throw new ArvoreException("Usuário não encontrado!");
    }

    /**
     *
     * @param no No recursivo de verificação de conteúdo
     * @param login Chave de busca na ABB
     * @return No no no derivado da busca
     * @throws ArvoreException Exceção emitida caso o usuário não esteja contido
     * na arvore
     */
    private No buscar(No no, String login) throws ArvoreException {
        if (no == null) {
            throw new ArvoreException("Usuário não encontrado!");
        }
        if (login.compareTo(no.getInfo().getLogin()) > 0) {
            return buscar(no.getDireita(), login);
        }
        if (login.compareTo(no.getInfo().getLogin()) < 0) {
            return buscar(no.getEsquerda(), login);
        }
        return no;
    }

    /**
     * Wrapper para inserir
     *
     * @param User Pessoa a ser adicionada
     */
    public void inserir(Usuario User) {
        try {
            this.raiz = inserir(this.raiz, User);

            salvarUsuarios();
        } catch (ArquivoException | ArvoreException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método para inserir o nó
     *
     * @param no No da arvore atual
     * @param user Informaçãoa ser adicionada
     * @return no No adicionado
     */
    private No inserir(No no, Usuario user) throws ArvoreException {
        if (no == null) {
            return new No(user);
        } else if (user.getLogin().compareTo(no.getInfo().getLogin()) > 0) {
            no.setDireita(inserir(no.getDireita(), user));
        } else if (user.getLogin().compareTo(no.getInfo().getLogin()) < 0) {
            no.setEsquerda(inserir(no.getEsquerda(), user));
        } else {
            throw new ArvoreException("Usuario existente");
        }
        return no;
    }

    /**
     * Wrapper para remover
     *
     * @param user da pessoa a ser adicionada
     * @return no No contendo a pessoa
     */
    public No remover(Usuario user) {
        No removido = null;
        try {
            removido = remover(this.raiz, user.getLogin());

            salvarUsuarios();
        } catch (ArquivoException | ArvoreException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        return removido;
    }

    /**
     * Chamada interna recursiva para remoÃ§Ã£o
     *
     * @param no No atual
     * @param login Nome do no a ser removido
     * @return No removido
     * @throws Exception excessão de arvore vazia
     */
    private No remover(No no, String login) throws ArvoreException{
        if (this.raiz == null) {
            throw new ArvoreException("Arvore Vazia!");
        } else {
            if (login.compareTo(no.getInfo().getLogin()) < 0) {
                no.setEsquerda(remover(no.getEsquerda(), login));
            } else if (login.compareTo(no.getInfo().getLogin()) > 0) {
                no.setDireita(remover(no.getDireita(), login));
            } else if (no.getEsquerda() != null && no.getDireita() != null) {
                no.setInfo(encontraMinimo(no.getDireita()));
                no.setDireita(removeMinimo(no.getDireita()));
            } else {
                no = (no.getEsquerda() != null) ? no.getEsquerda() : no.getDireita();
            }
            return no;
        }
    }

    /**
     * Chamada recursiva para remover o menor no
     *
     * @param no No a ser percorrido
     * @return No removido
     */
    private No removeMinimo(No no) {
        if (no == null) {
            return null;
        } else if (no.getEsquerda() != null) {
            no.setEsquerda(removeMinimo(no.getEsquerda()));
            return no;
        } else {
            return no.getDireita();
        }
    }

    /**
     *
     * @param no No percorrido para encontrar o menor a partir dele
     * @return Pessoa dados do menor nÃ³ encontrado
     */
    private Usuario encontraMinimo(No no) {
        if (no != null) {
            while (no.getEsquerda() != null) {
                no = no.getEsquerda();
            }
        }
        return no.getInfo();
    }

    /**
     * Wrapper para busca Préfixa
     */
    public void buscaPreFixa() {
        buscaPreFixa(raiz);
    }

    /**
     * Chamada recursiva para busca Prefixada
     *
     * @param noBusca no atual da busca
     */
    private void buscaPreFixa(No noBusca) {
        if (noBusca != null) {
            System.out.println("Login.: " + noBusca.getInfo().getLogin());
            System.out.println("Nome.: " + noBusca.getInfo().getNome());
            System.out.println("Senha.: " + noBusca.getInfo().getSenha());
            System.out.println("\n");
            buscaPreFixa(noBusca.getEsquerda());
            buscaPreFixa(noBusca.getDireita());
        }
    }

    /**
     * Wrapper para salvar no arquivo .txt os nomes do usuário
     *
     * @throws imd.lab.Exception.ArquivoException exceção emitida caso sistema
     * não consiga salvar dados do usuário
     */
    public void salvarUsuarios() throws ArquivoException {
        FileWriter arq;
        try {
            arq = new FileWriter("lib\\usuarios.txt");
            PrintWriter gravarArq = new PrintWriter(arq);
            salvarUsuarios(gravarArq, raiz);
            arq.close();
        } catch (IOException ex) {
            throw new ArquivoException("Falha ao salvar Usuario");
        }
    }

    /**
     * Função privada para salvar recursivamente o nome, login e senha dos
     * usuários
     *
     * @param gravarArq Arquivo em que está sendo gravado os dados do usuário
     * @param noBusca No atual que possui os dados
     */
    private void salvarUsuarios(PrintWriter gravarArq, No noBusca) {
        if (noBusca != null) {
            gravarArq.println(noBusca.getInfo().getLogin());
            gravarArq.println(noBusca.getInfo().getNome());
            gravarArq.println(noBusca.getInfo().getSenha());
            gravarArq.println(noBusca.getInfo().getClass().getSimpleName());
            salvarUsuarios(gravarArq, noBusca.getEsquerda());
            salvarUsuarios(gravarArq, noBusca.getDireita());
        }
    }
}

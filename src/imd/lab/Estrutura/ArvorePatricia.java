package imd.lab.Estrutura;

import java.util.Scanner;

public class ArvorePatricia
{  
        private char palavraChar[];
        protected NoPatricia raiz;
        protected int numDeNodos;
        protected Scanner leitor;
        
	public ArvorePatricia(int numDeNos){
            this.numDeNodos = numDeNos;
            raiz = new NoPatricia(numDeNos);
            raiz.isFinal = false;
            raiz.numeroDePrefixo = 0;
            raiz.prev = null;
	}

	//Executa as ações
	public void iniciar(String stringInserir[]){
            StringBuilder stringAcaoPalavra = new StringBuilder();
            int i = 0;
	do{	//Converte a string pra um array de char
                stringAcaoPalavra.append(stringInserir);
		palavraChar = new char[stringAcaoPalavra.length()];
		palavraChar = stringAcaoPalavra.toString().toCharArray();
			/*
			 * De acordo com cada comando que se encontra na primeira
			 * 	posicao executa a ação.
			 * 	Ententendo: a palavra é passada para o inserir que retorna
			 * 	 um boolean sobre a ação que é repassado para o gerador de
			 * 	 saída que grava arquivo saida.txt
			 */
                        switch(palavraChar[0])
                        {
                            case 'i':
                                        geradorDeSaida( inserirString( palavraChar ) );
                                        break;
                            case 'b':
                                        geradorDeSaida( buscarString( palavraChar ) );
                                        break;
                            case 'r':
                                        geradorDeSaida( removerString( palavraChar ) );
                                        break;
                            case 'c':
                            			completar(palavraChar);
                            			break;
                            default:
                                         finalizar();   
                        }
			stringAcaoPalavra.delete(0, stringAcaoPalavra.length());
                        i++;
        }
        while(stringInserir.length >= i);
        }
        
	
	
	public void finalizar()
	{
                raiz = null;
                System.gc();
                System.runFinalization();;
                System.exit(0);
	}
        
	/**
	 * Método que insere uma string dentro da trie
	 * @param Char[] string
	 * @return Boolean {true - se inseriu ou há o elemento já inserido | false - se não pode inserir na árvore }
	 */
	protected boolean inserirString(char[] string)
	{
		NoPatricia nodo = raiz;
		nodo.numeroDePrefixo++;	//Diz que ali tem mais um nodo

		for( int a = 2; a < string.length; a++)
		{			
			int intChar = this.posicaoDoChar(string[a]);
			if( nodo.nodos[ intChar ] == null )
			{	
				try{
					nodo.nodos[ intChar ] = new NoPatricia(numDeNodos);
                }
                catch(Exception erro)
                {
                	System.gc();
                    System.runFinalization();
                }

              
				if( nodo.nodos[ intChar ].nodos == null )
                {
					return false;
                }
				nodo.sub = nodo.nodos[intChar];
				nodo.nodos[ intChar ].prev = nodo;
                nodo.nodos[intChar].valor = string[a];
                
			}
			nodo.nodos[ intChar ].numeroDePrefixo++;
			nodo = nodo.nodos[ intChar ];
		}
		nodo.isFinal = true;
		return nodo.isFinal;
	}
        /*
	 * @param Char[] string
	 * @return boolean { true - se removeu | false - se não removeu }
	 */
	protected boolean removerString(char[] string)
	{
		NoPatricia nodo = raiz;
		/*
		 * Primeiro passo do remover é buscar a string até o final. 
		 * Isso faz com que o nodo esteja no nodo mais distante da string
		 * 	para que se possa voltar deletando
		 */
		int a;
		for( a = 2; a < string.length; a++)
		{
			if( nodo.nodos[ this.posicaoDoChar( string[a] ) ] != null )
			{
				nodo = nodo.nodos[ this.posicaoDoChar( string[a] ) ];
			}
			else
			{
				return false;
			}
		}
                
		/*
		 * Segundo passo é marcar esse nodo como não final, pois
		 * 	estaremos removendo a palavra que está até aqui. Depois
		 * 	enquanto ele não tiver prev null (raiz)
		 */
                if( nodo.isFinal != true)
                {
                    return false;
                }
		nodo.isFinal = false;
		NoPatricia temp;
		
		while( nodo.prev != null)
		{
			if( nodo.numeroDePrefixo <= 0)
			{
				temp = nodo;
				nodo = nodo.prev;
				nodo.numeroDePrefixo--;
				temp.finalize();
			}
			else
			{
				nodo = nodo.prev;
			}
		}
                /**
                 * Se o nodo é igual a raiz, então houve sucesso
                 *  no retornar
                 */
		if( nodo.equals(raiz))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	/**
	 * Método para realizar busca da string dentro
	 * 	da árvore trie
	 * 
	 * @param Char[] string
	 * @return boolean { true - se achou | false - se não achou }
	 */
	protected boolean buscarString(char[] string)
	{
		NoPatricia nodo = raiz;
		
		for( int a = 2; a < string.length; a++)
		{
			if( nodo.nodos[ this.posicaoDoChar( string[a] ) ] != null)
			{
				nodo = nodo.nodos[ this.posicaoDoChar( string[a] ) ];
			}
			else
			{
				return false;
			}
		}
		return nodo.isFinal;
	}
	
	
	protected void completar(char[] string){
		int a;
		NoPatricia nodo = raiz;
		boolean matches = true;
		for(a = 2; a < string.length; a++)
		{
			if( nodo.nodos[ this.posicaoDoChar( string[a] ) ] != null)
			{
				nodo = nodo.nodos[ this.posicaoDoChar( string[a] ) ];
			}
			else
			{
				matches = false;
				System.out.println("Essa música não está na lista");
			}
		}
		
		if (matches == true){
			NoPatricia copia = nodo;
			
			for(int subarv = 0; subarv < nodo.numeroDePrefixo; subarv++){
	
				for (int i = 2; i < string.length; i++)
					System.out.print(string[i]);

				do {
					
					nodo = nodo.sub;
					System.out.print(nodo.valor);	
				
				}while(nodo.isFinal == false);
	
				System.out.println("");
				nodo = copia;
			}
		}
	}
	
	/**
	 * Recebe um boolean que faz com que seja imprimido
	 *  V ou F de acordo com o seu valor
	 * @param boolean saida
	 */
	protected void geradorDeSaida(boolean saida)
	{
		if(saida)
		{
			System.out.println("v");
		}
		else
		{
			System.out.println("f");
		}
	}
	
	/**
	 * O método retorna o código que representa o char
	 * 	de 0 a 25 que será a sua posição na árvore trie
	 * @param char letra
	 * @return int{0 - 25}
	 */
	public int posicaoDoChar(char letra)
	{
		return (int)letra - 97;
	}
	
}
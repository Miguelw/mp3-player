package imd.lab.Estrutura;

public class NoPatricia {
	protected char valor;
	protected boolean isFinal;
	protected NoPatricia nodos[];
	protected int numeroDePrefixo;
	protected NoPatricia prev;
	protected NoPatricia sub;

	
	public NoPatricia(int numDeNodos)
	{
		isFinal = false;
		nodos = new NoPatricia[numDeNodos];
		numeroDePrefixo = 0;
		prev = null;
		sub = null;
		valor = 'z';
	}   
        @Override
	protected void finalize()
	{
                isFinal = false;
	}

}
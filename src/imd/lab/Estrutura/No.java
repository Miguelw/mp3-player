package imd.lab.Estrutura;

import imd.lab.Controle.Usuario;

public class No {

    private Usuario info;
    private No esquerda;
    private No direita;

    /**
     *
     * @param valor Pessoa a ser adicionada no No
     */
    public No(Usuario valor) {
        this.info = valor;
        this.esquerda = null;
        this.direita = null;
    }

    public Usuario getInfo() {
        return info;
    }

    public void setInfo(Usuario info) {
        this.info = info;
    }

    public No getEsquerda() {
        return esquerda;
    }

    public void setEsquerda(No esquerda) {
        this.esquerda = esquerda;
    }

    public No getDireita() {
        return direita;
    }

    public void setDireita(No direita) {
        this.direita = direita;
    }
}

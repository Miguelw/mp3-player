package imd.lab.Dao;

import imd.lab.Exception.ArquivoException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author sarah
 */
public class DiretorioDao {

    private ArrayList<String> diretorios;
    private static DiretorioDao instance;

    private DiretorioDao() {
        diretorios = new ArrayList<>();
    }

    public static synchronized DiretorioDao getInstance() {
        if (instance == null) {
            instance = new DiretorioDao();
        }
        return instance;
    }

    public void carregarDiretorios() throws ArquivoException {
        try (FileReader arq = new FileReader("lib\\diretorios.txt")) {
            BufferedReader diretorioAtual = new BufferedReader(arq);
            while (diretorioAtual.ready()) {
                diretorios.add(diretorioAtual.readLine());
            }
        } catch (IOException e) {
            throw new ArquivoException("Erro na abertura do arquivo");
        }
    }

    private void salvarDiretorios() throws ArquivoException {

        FileWriter arq;
        try {
            arq = new FileWriter("lib\\diretorios.txt");
            PrintWriter gravarArq = new PrintWriter(arq);
            diretorios.stream().forEach((diretorio) -> {
                gravarArq.println(diretorio);
            });
            arq.close();
        } catch (IOException ex) {
            throw new ArquivoException("Erro ao salvar diretórios");
        }
    }

    public void adicionarDiretorio(String diretorioAtual) {
        diretorios.add(diretorioAtual);
        try {
            salvarDiretorios();
        } catch (ArquivoException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }

    public boolean removerDiretorio(String diretorioAtual) {
        boolean removido = diretorios.remove(diretorioAtual);
        try {
            salvarDiretorios();
        } catch (ArquivoException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        return removido;
    }

    public ArrayList<String> getDiretorios() {
        return this.diretorios;
    }
}

package imd.lab.Dao;

import imd.lab.Controle.Usuario;
import imd.lab.Controle.UsuarioPadrao;
import imd.lab.Controle.UsuarioPremium;
import imd.lab.Estrutura.ArvoreBinaria;
import imd.lab.Exception.ArquivoException;
import imd.lab.Exception.ArvoreException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import imd.lab.Estrutura.ArvorePatricia;

public class UsuarioDao {

    private ArvoreBinaria usuarios;
     private ArvorePatricia playlist;
    private static UsuarioDao instance;
    private ArrayList<String> diretorios;

    private UsuarioDao() {
        usuarios = new ArvoreBinaria();
    }

    public static synchronized UsuarioDao getInstance() {
        if (instance == null) {
            instance = new UsuarioDao();
        }
        return instance;
    }

    public void adicionarUsuario(Usuario user) {
        usuarios.inserir(user);
    }

    public Usuario buscarUsuario(String login, String senha) {
        try {
            return usuarios.buscar(login, senha).getInfo();
        } catch (ArvoreException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    public boolean removerUsuario(Usuario user) {

        return usuarios.remover(user) != null;

    }

    public ArvoreBinaria getUsuarios() {
        return this.usuarios;
    }

     public void carregarPlaylist(Usuario user) {
        String nomeArquivo;
        nomeArquivo = user.getLogin().toLowerCase().concat(user.getNome().toLowerCase());
        File arquivos[];
        File diretorio;
        diretorio = new File("lib\\");
        arquivos = diretorio.listFiles();
        for (File arquivo : arquivos) {
            if (arquivo.toString().toLowerCase().contains(nomeArquivo)) {
                ((UsuarioPremium) user).adicionarPlaylist((arquivo.toPath().getFileName()).toString().replace(".txt", ""));
            }
        }
    }

    public ArrayList<String> getDiretorios() {
        return this.diretorios;
    }

    public void carregarUsuarios() throws ArquivoException {
        String nome, login, senha, tipo;
        try (FileReader arq = new FileReader("lib\\usuarios.txt")) {
            BufferedReader listaUsuarios = new BufferedReader(arq);
            while (listaUsuarios.ready()) {
                login = listaUsuarios.readLine();
                nome = listaUsuarios.readLine();
                senha = listaUsuarios.readLine();
                tipo = listaUsuarios.readLine();
                if (tipo.equals("UsuarioPadrao")) {
                    usuarios.inserir(new UsuarioPadrao(nome, login, senha));
                } else {
                    usuarios.inserir(new UsuarioPremium(nome, login, senha));
                }
            }
        } catch (IOException e) {
            throw new ArquivoException("Erro na abertura do arquivo");
        }
    }
}

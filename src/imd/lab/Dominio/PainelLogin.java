package imd.lab.Dominio;

import imd.lab.Controle.Usuario;
import imd.lab.Dao.UsuarioDao;
import imd.lab.Dao.DiretorioDao;
import imd.lab.Exception.ArquivoException;
import imd.lab.FabricaAbstrata.FabricaAbstrata;
import javax.swing.JOptionPane;

/**
 *
 * @author Miguel
 */
public class PainelLogin extends javax.swing.JFrame {

    UsuarioDao usuarios = UsuarioDao.getInstance();
    DiretorioDao diretorios = DiretorioDao.getInstance();

    public PainelLogin() {
        initComponents();
        try {
            usuarios.carregarUsuarios();
            diretorios.carregarDiretorios();
        } catch (ArquivoException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        buttonLogin = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        TextUser = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        TextKey = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(25, 25, 25));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        buttonLogin.setBackground(new java.awt.Color(33, 33, 33));
        buttonLogin.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        buttonLogin.setForeground(new java.awt.Color(204, 204, 204));
        buttonLogin.setText("Login");
        buttonLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonLoginActionPerformed(evt);
            }
        });
        jPanel1.add(buttonLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 180, 190, -1));

        jLabel1.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 204, 204));
        jLabel1.setText("Usuário");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, -1, -1));

        TextUser.setBackground(new java.awt.Color(39, 39, 39));
        TextUser.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        TextUser.setForeground(new java.awt.Color(204, 204, 204));
        jPanel1.add(TextUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 370, 30));

        jLabel2.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(204, 204, 204));
        jLabel2.setText("Senha");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, -1, -1));

        TextKey.setBackground(new java.awt.Color(39, 39, 39));
        TextKey.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        TextKey.setForeground(new java.awt.Color(204, 204, 204));
        jPanel1.add(TextKey, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 370, 30));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 300));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonLoginActionPerformed
        Usuario user;
        try {
            user = usuarios.buscarUsuario(TextUser.getText(), String.valueOf(TextKey.getPassword()));
            setVisible(false);
            FabricaAbstrata.getInstance(user).visualizarTela();
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        }


    }//GEN-LAST:event_buttonLoginActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PainelLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PainelLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PainelLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PainelLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PainelLogin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField TextKey;
    private javax.swing.JTextField TextUser;
    private javax.swing.JButton buttonLogin;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

}

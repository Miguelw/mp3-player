package imd.lab.FabricaAbstrata;

import imd.lab.Controle.Usuario;
import imd.lab.Dominio.PainelTelaInicial;

public class FabricaConcretaPremium extends FabricaAbstrata {

    Usuario user;

    FabricaConcretaPremium(Usuario user) {
        this.user = user;
    }

    @Override
    public void visualizarTela() {
        new PainelTelaInicial(user).setVisible(true);
    }

}

package imd.lab.FabricaAbstrata;

import imd.lab.Controle.Usuario;

public abstract class FabricaAbstrata {

    public abstract void visualizarTela();

    public static FabricaAbstrata getInstance(Usuario user) {
        if (user.getClass().getSimpleName().equals("UsuarioPadrao")) {
            return new FabricaConcretaPadrao(user);
        } else if (user.getClass().getSimpleName().equals("UsuarioPremium")) {
            return new FabricaConcretaPremium(user);
        }
        return null;
    }
}

package imd.lab.FabricaAbstrata;


import imd.lab.Controle.Usuario;
import imd.lab.Dominio.PainelTelaInicial;

public class FabricaConcretaPadrao extends FabricaAbstrata {

    Usuario user;
    
    FabricaConcretaPadrao(Usuario user) {        
        this.user=user;
    }

    @Override
    public void visualizarTela() {
        new PainelTelaInicial(user).setVisible(true);
    }
    
}

Media Player com Java
O projeto consiste em um tocador de MP3 utilizando a biblioteca JLayer
(http://www.javazoom.net/javalayer/javalayer.html), capaz de produzir arquivos de
áudio. Outras bibliotecas de áudio podem ser utilizadas. É necessário que os alunos
utilizem conceitos dados na disciplina de LP2, como:
1. Organização de pacotes;
2. Interface (GUI) Swing;
3. Utilização de bibliotecas externas;
4. Documentação JavaDOC;
5. Herança;
6. Polimorfismo;
7. Classe Abstrata;
8. Interfaces;
9. Classes para tratamento de Exceções.